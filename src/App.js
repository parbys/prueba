import axios from 'axios';
import React, {useState, useEffect} from 'react'
import img from './componets/images/widget.png'
import './App.css'
import {ip} from './ApiRest';

function App() {
  ///apis keys
  const ApiCli = "b118aacecba4ad13846156ee434b18ee"
  const ApiNot = "a05f8b385bd14cb590325c8d491cece6"

  ///variable de estado
  const [estado, setEstado] = useState(false)

  ///
  const [ historia, setHistoria ] = useState([])

  const [ noticiasList, setNoticiasLis ] = useState([])
  const [ estadolist, setEstadolist ] = useState(null)
  const [ valor, setValor ] = useState({
    nombre: '',
    info: 'datos'
  })
  useEffect(()=>{
    obtain()
  },[])

  const backUrl = "https://localhost:44382/api/values";

/// Funcion para obtener los datos de la api C#
  const obtain = async () =>{
    await axios.get(backUrl).then(response=>{
      console.log("entroo a la funciòn");
      console.log(response.data);
      setHistoria(response.data)
      setValor({
        nombre:'',
        info:'datos'
      })
    })
  }

//Funcion Guardar este resive datos a guardar de forma de json
  const agregar = async () =>{
    await axios.post(backUrl,valor).then(response=>{
      console.log("entroo a la funciòn");
      console.log(response.data);
      obtain()
    })
  }

  ///funcion para buscar datos del tiempo metereologico
  const search = () =>{
    var text = valor.nombre
    axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${text}&appid=${ApiCli}&units=metric`)
    .then(response =>{
      setEstadolist(response.data)
      noticia(response.data.sys.country)
      console.log(response);
      setValor({
        nombre:'',
        info:'datos'
      })
      // setValor({})
      // setValor({
      //   nombre:'',
      //   info:''
      // })
      })
  }


//funcion para buscar noticias segun el pais
  const noticia = (value) =>{
    axios.get(`https://newsapi.org/v2/top-headlines?country=${value}&apiKey=${ApiNot}`)
     .then(result =>{
       setNoticiasLis(result.data.articles)
       //se le pasa la funcion agregar esta me guarda la consulta realizada
       agregar()
    })

  }

  return(
    <div>
      <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"/>
          <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />

      </head>

      <header style={{border:'5%'}} id="top">
          <div class="row" >
              <div class="col-md-12" style={{boxShadow:'0px 20px 60px #55575a', padding:'auto', height:55}}>
                  <div class="head d-flex mt-1">
                    <div class="col-md-2">
                      <i class="fas fa-history m-3" onClick={()=>setEstado(!estado)}>   Historial consulta </i>
                      {
                        estado == true ?
                        <ul style={{backgroundColor:'white'}}>
                          {
                          historia.map((item)=>{
                            return(
                              <li class="mt-1">{item.nombre}</li>
                            )
                          })
                        }
                       </ul>
                       :null
                      }

                    </div>
                        <div class="col-md-10 justify-content-end">
                          <div class="justify-content-end" style={{marginLeft:'15%'}}>
                            <input name="nombre" id="nombre"  onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })} valor={valor.nombre} class="col-md-6"
                              style={{borderRadius:15, height: 40, border:'1px solid #D9D9D9 ', backgroundColor:'white',borderRadius:30, marginTop:2}}
                             placeholder="Consulta Ciudad"/>
                            <button type="button" class="btn btn-info" onClick={()=>search()} style={{borderRadius:12, marginLeft:34, marginTop:'-10px'}}> Buscar </button>

                          </div>
                        </div>
                  </div>
              </div>
          </div>
      </header>

        <div class="container">
           <div class="row justify-content-center mt-5" style={{marginLeft:40}}>
            <div class="col-md-6">
              <h4 class="text-center">Noticias</h4>
              <div class="scroll mt-3">
              {
                noticiasList.map((item,key)=>{
                  return(
                    <div class="card d-flex align-items-center mt-3" style={{width:'15rem;', boxShadow:'0px 4px 5px #55575a;', borderRadius:12}}>
                        <h5 class="card-title">{item.title}</h5>
                        <img src={item.urlToImage} class="img-fluid" alt="Responsive image"/>
                        <div class="card-body">
                          <h5 class="card-title">{item.author}</h5>
                          <p class="card-text">{item.description}</p>
                        </div>
                    </div>

                  )
                })
              }
              </div>
             </div>
             <div class="col-md-6">
               <h4 class="text-center">Estado del tiempo</h4>
               {
                 estadolist != null ?
                 <div class="card d-flex align-items-center justify-content-center pt-5 mt-3 col-md-5" style={{ boxShadow:'0px 4px 5px #55575a;', borderRadius:12, marginLeft:'30%'}}>
                      <h5 class="card-title">{estadolist.name}</h5>
                       <div class="card-body">
                         <i class="fas fa-cloud-sun" style={{fontSize:48}}></i>
                         <p>{estadolist.main.feels_like} °C</p>
                       </div>
                   </div>

                   :null
               }

              </div>


        </div>
    </div>
  </div>
  );
}

export default App;
